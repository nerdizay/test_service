from django.shortcuts import redirect, render


def index(request):
    if not request.user.is_authenticated:
        return render(request, "do_you_have_an_account.html")

    return redirect("tests")
