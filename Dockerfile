FROM python:3.10

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		postgresql-client \
	&& rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY . .
# RUN pip install poetry
# RUN poetry shell
# RUN poetry install
# COPY requirements.txt ./
RUN pip install -r requirements.txt
# COPY . .

EXPOSE 8000
# CMD ["python", "manage.py", "migrate", "--noinput"]
# CMD ["psql", "manage.py", "runserver", "0.0.0.0:8000"]
