from django.apps import AppConfig


class TestQuestionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'test_questions'
