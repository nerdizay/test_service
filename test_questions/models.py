from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class CustomBaseModel:
    @classmethod
    def get_by_id(cls, id):
        try:
            obj = cls.objects.get(id=id)
        except ObjectDoesNotExist:
            return None
        else:
            return obj


class Tests(models.Model, CustomBaseModel):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=200, verbose_name="название")
    description = models.CharField(default="Нет описания", verbose_name="описание")
    is_active = models.BooleanField(default=True, verbose_name="активен?")

    class Meta:
        verbose_name = "тест"
        verbose_name_plural = "тесты"
        ordering = ["id"]

    def __str__(self):
        return self.name


class Questions(models.Model, CustomBaseModel):
    id = models.BigAutoField(primary_key=True)
    question = models.CharField(verbose_name="вопрос")
    id_test = models.ForeignKey(Tests, on_delete=models.CASCADE, verbose_name="тест")

    class Meta:
        verbose_name = "вопрос"
        verbose_name_plural = "вопросы"

    def __str__(self):
        return self.question

    @staticmethod
    def get_next(session):
        try:
            history_tests = (
                HistoryTests.objects.filter(session=session, done=False, skip=False)
                .all()
                .order_by("question_id")[0]
            )
            return history_tests.question
        except IndexError:
            try:
                history_tests = (
                    HistoryTests.objects.filter(session=session, done=False, skip=True)
                    .all()
                    .order_by("question_id")[0]
                )
                return history_tests.question
            except IndexError:
                return None


class Answers(models.Model):
    id = models.BigAutoField(primary_key=True)
    answer = models.CharField(verbose_name="ответ")
    is_correct = models.BooleanField(verbose_name="правильный?")
    id_question = models.ForeignKey(
        Questions, on_delete=models.CASCADE, verbose_name="вопрос"
    )

    class Meta:
        verbose_name = "ответ"
        verbose_name_plural = "ответы"

    def __str__(self):
        return self.answer

    @staticmethod
    def get_correct_version(question_id):
        answers = Answers.objects.filter(id_question=question_id).all()
        version_creator = {"good": [], "bad": []}
        for answer in answers:
            if answer.is_correct:
                version_creator["good"].append(answer.id)
            else:
                version_creator["bad"].append(answer.id)
        return version_creator

    @staticmethod
    def get_by_question(question):
        return Answers.objects.all().filter(id_question=question)

    @staticmethod
    def accept_answers(
        session, version_user, version_creator, user_id, test_id, question
    ) -> None:
        count_answers = len(version_creator["bad"]) + len(version_creator["good"])
        count_sameness = 0
        for answer_good in version_creator["good"]:
            if answer_good in version_user["good"]:
                count_sameness += 1
        for answer_bad in version_creator["bad"]:
            if answer_bad in version_user["bad"]:
                count_sameness += 1
        correct_percent = count_sameness / count_answers

        HistoryTests.objects.filter(
            session=session, id_test=test_id, id_user=user_id, question=question
        ).update(done=True, correct_value=correct_percent)


class TestSessions(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)

    @staticmethod
    def get_or_open(user, test):
        try:
            open_session = TestSessions.objects.get(id_user=user)
        except ObjectDoesNotExist:
            open_session = TestSessions(id_user=user)
            open_session.save()
            history = HistoryTests.prepare(open_session, test, user)
            return open_session
        else:
            return open_session

    @staticmethod
    def close(user):
        TestSessions.objects.filter(id_user=user).delete()

    @staticmethod
    def get_full_data_session(user, test_id, question_id):
        test = Tests.get_by_id(test_id)
        session = TestSessions.get_or_open(user, test)
        progress = HistoryTests.get_progress(session)
        current_question = Questions.get_by_id(question_id)
        answers = Answers.get_by_question(current_question)
        next_question = Questions.get_next(session)

        return {
            "test": test,
            "session": session,
            "progress": progress,
            "question": current_question,
            "answers": answers,
            "next_question": next_question,
        }


class HistoryTests(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_test = models.ForeignKey(Tests, on_delete=models.CASCADE)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)
    session = models.ForeignKey(TestSessions, null=True, on_delete=models.SET_NULL)
    question = models.ForeignKey(Questions, null=True, on_delete=models.CASCADE)
    done = models.BooleanField(default=False)
    correct_value = models.FloatField(null=True)
    skip = models.BooleanField(default=False)

    class Meta:
        verbose_name = "история"
        verbose_name_plural = "истории тестов"
        

    @staticmethod
    def get_progress(session):
        done_questions = HistoryTests.objects.filter(session=session, done=True).all()
        not_done_question = HistoryTests.objects.filter(
            session=session, done=False
        ).all()
        
        done = len(done_questions)
        not_done = len(not_done_question)
        all = done + not_done
        return done, all

    @staticmethod
    def prepare(session, test, user) -> str:
        questions = Questions.objects.filter(id_test=test).all()
        history_tests = HistoryTests.objects.bulk_create(
            [
                HistoryTests(
                    id_test=test, id_user=user, session=session, question=question
                )
                for question in questions
            ]
        )
        return history_tests

    @staticmethod
    def get_result_test(test, user, session):
        history_tests = HistoryTests.objects.filter(
            id_test=test, id_user=user, session=session
        )
        count_questions = len(history_tests)
        general_correct_value = 0
        for history_test in history_tests:
            general_correct_value += history_test.correct_value
        return f"{general_correct_value*100/count_questions:.0f}"
