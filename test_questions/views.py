from django.shortcuts import redirect, render
from .models import Tests, Questions, Answers, HistoryTests, TestSessions
from django.views.generic import ListView
from django.views.generic import TemplateView


class ChooseTestListView(ListView):
    paginate_by = 10
    model = Tests
    template_name = "choose_test.html"
    
    ordering = ['-id']


class PresentationTestView(TemplateView):
    template_name = "presentation_test.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        test_id = kwargs.get("test_id")
        test = Tests.objects.get(id=test_id)
        session = TestSessions.get_or_open(self.request.user, test)
        done, all = HistoryTests.get_progress(session)
        question = Questions.get_next(session)
        context["test"] = test
        context["next_question"] = question
        context["done"] = done
        context["all"] = all
        return context
    

class ChooseAnswerView(TemplateView):
    template_name = "choose_answer.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        test_id = kwargs.get("test_id")
        question_id = kwargs.get("question_id")
        test = Tests.get_by_id(test_id)
        session = TestSessions.get_or_open(self.request.user, test)
        current_question = Questions.get_by_id(question_id)
        next_question = Questions.get_next(session)
        answers = Answers.get_by_question(current_question)
        done, all = HistoryTests.get_progress(session)
        context["test"] = test
        context["question"] = current_question
        context["answers"] = answers
        context["next_question"] = next_question
        context["done"] = done
        context["all"] = all
        return context

    def post(self, request, test_id, question_id, *args, **kwargs):
        answers_id = [
            int(key.split("-")[-1])
            for key in request.POST.keys()
            if key != "csrfmiddlewaretoken"
        ]
        if not answers_id:
            return redirect(f"/tests/{test_id}/questions/{question_id}")
        test = Tests.objects.get(id=test_id)
        session = TestSessions.get_or_open(self.request.user, test)
        question = Questions.objects.get(id=question_id)
        answers = Answers.objects.all().filter(id_question=question)
        version_user = {}
        version_user["good"] = answers_id
        version_user["bad"] = list(set([answer.id for answer in answers if not answer.is_correct]).difference(answers_id))
        version_creator = Answers.get_correct_version(question_id)
        Answers.accept_answers(session, version_user, version_creator, self.request.user.id, test_id, question)
        next_question = Questions.get_next(session)
        if not next_question:
            return redirect(f"/tests/{test_id}/done")
        return redirect(f"/tests/{test_id}/questions/{next_question.id}")

class EndTestView(TemplateView):
    template_name = "end_test.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        test_id = kwargs.get("test_id")
        test = Tests.get_by_id(test_id)
        session = TestSessions.get_or_open(self.request.user, test)
        correct_value = HistoryTests.get_result_test(test, self.request.user, session)
        TestSessions.close(self.request.user)
        context["test"] = test
        context["correct_value"] = correct_value
        return context
    
    
def skip_question(request, test_id, question_id, *args, **kwargs):
    
    test = Tests.objects.get(id=test_id)
    session = TestSessions.get_or_open(request.user, test)
    question = Questions.get_by_id(question_id)
    HistoryTests.objects.filter(
        id_test=test, id_user=request.user, session=session, question=question
    ).update(skip=True)
    next_question = Questions.get_next(session)
    return redirect(f"/tests/{test_id}/questions/{next_question.id}")
    
    