from django.contrib import admin
from test_questions.models import Tests, Questions, Answers, HistoryTests
from django.views import View


@admin.register(Tests)
class TestsAdmin(admin.ModelAdmin):
    list_display = ["name", "description", "is_active"]
    # exclude = ["count_questions"]
    
    # @admin.display(description="название теста")
    # def name_(self, obj):
    #     return f"{obj.name}"
    
    # @admin.display(description="описание")
    # def description_(self, obj):
    #     return f"{obj.description}"
    
    # @admin.display(description="активен?", boolean=True)
    # def is_active_(self, obj):
    #     return obj.is_active


@admin.register(Questions)
class QuestionsAdmin(admin.ModelAdmin):
    list_display = ["question", "test_"]
    search_fields = ["question"]
    
    @admin.display(description="тест")
    def test_(self, obj):
        test = Tests.objects.get(id=obj.id_test_id)
        return f"{test.name}"
    
    # @admin.display(description="вопрос", ordering='-question')
    # def question_(self, obj):
    #     return f"{obj.question}"
    # ordering = ["question"]
    


@admin.register(Answers)
class AnswersAdmin(admin.ModelAdmin):
    list_display = ["answer_", "is_correct_", "id_question_"]
    search_fields = ["answer"]
    
    @admin.display(description="вариант ответа")
    def answer_(self, obj):
        return f"{obj.answer}"
    
    @admin.display(description="это правильный ответ?", boolean=True)
    def is_correct_(self, obj):
        return obj.is_correct
    
    @admin.display(description="вопрос")
    def id_question_(self, obj):
        question = Questions.objects.get(id=obj.id_question_id)
        return f"{question.question}"


@admin.register(HistoryTests)
class HistoryTestsAdmin(admin.ModelAdmin):
    list_display = ["id_test", "id_user", "session", "question", "done", "correct_value"]
    

admin.site.site_title = "Cервис создания тестов"
admin.site.site_header = "Админ-панель сервиса создания тестов"