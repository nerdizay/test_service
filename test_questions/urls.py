from django.urls import path
# from django.views.decorators.cache import cache_page

from .views import (
    ChooseTestListView,
    EndTestView,
    # presentation_test,
    # choose_answer
    PresentationTestView,
    ChooseAnswerView,
    skip_question
)

urlpatterns = [
    # path('', views.choose_test, name='tests'),
    path('', ChooseTestListView.as_view(), name="tests"),
    path('<int:test_id>/', PresentationTestView.as_view(), name="presentation_test"),
    path('<int:test_id>/done/', EndTestView.as_view(), name="end_test"),
    path('<int:test_id>/questions/<int:question_id>/', ChooseAnswerView.as_view(), name="choose_answer"),
    path('<int:test_id>/questions/<int:question_id>/skip/', skip_question, name="skip_question"),
]