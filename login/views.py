from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.tokens import default_token_generator
from .forms import LoginForm, RegisterForm
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from .utils import custom_send_mail


class CustomLoginView(TemplateView):
    template_name = "login.html"
    success_url = reverse_lazy("tests")

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("tests")

        form = LoginForm()
        form["username"].label = "логин"
        form["password"].label = "пароль"
        return render(
            request,
            self.template_name,
            context={
                "form": form,
            },
        )

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("tests")

        form = LoginForm(request.POST)
        user = authenticate(
            request,
            username=form.data.get("username"),
            password=form.data.get("password"),
        )
        if user is not None:
            login(request, user)
            return redirect("tests")

        form["username"].label = "логин"
        form["password"].label = "пароль"
        return render(
            request,
            self.template_name,
            context={"form": form, "error": "Пользователь не был найден"},
        )


class CustomLogoutView(TemplateView):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("login")

        logout(request)
        return redirect("login")


class CustomSignUpView(TemplateView):
    template_name = "sign-up.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("tests")

        form = RegisterForm()
        form["username"].label = "логин"
        form["password1"].label = "пароль"
        form["password2"].label = "пароль"
        form["email"].label = "email"
        return render(
            request,
            self.template_name,
            context={
                "form": form,
            },
        )

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("tests")

        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            token = default_token_generator.make_token(user)
            custom_send_mail(request, user.id, user.email, token)
            return redirect("waiting-confirm")

        form = RegisterForm()
        return render(
            request, self.template_name, context={"form": form, "error": "Ошибка"}
        )


class WaitingConfirm(TemplateView):
    template_name = "check_email.html"


def confirm_email(request, user_id, token):
    try:
        user = User.objects.get(id=user_id)
    except ObjectDoesNotExist:
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return redirect("tests")

    return redirect("sign-up")
