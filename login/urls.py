from django.urls import path

from .views import CustomLoginView, CustomLogoutView, CustomSignUpView, WaitingConfirm, confirm_email

urlpatterns = [
    path("login/", CustomLoginView.as_view(), name="login"),
    path("logout/", CustomLogoutView.as_view(), name="logout"),
    path("sign-up/", CustomSignUpView.as_view(), name="sign-up"),
    path("waiting-confirm/", WaitingConfirm.as_view(), name="waiting-confirm"),
    path("confirm/<int:user_id>/<str:token>", confirm_email, name="confirm"), 
]