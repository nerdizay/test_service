from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.conf import settings

TITLE_EMAIL = "Регистрация на сервисе тестов"
PROTOCOL = "http"
URL_CONFIRM = "{}://{}/auth/confirm/{}/{}"
MESSAGE_CONFIRM = """Пожалуйста, перейдите по следующей ссылке, 
чтобы подтвердить свой адрес электронной почты: {}"""

      
def custom_send_mail(
    request,
    user_id,
    user_email,
    token
):
    site = get_current_site(request)
    with mail.get_connection() as connection:
        url = URL_CONFIRM.format(PROTOCOL, site, user_id, token)
        mail.EmailMessage(
            TITLE_EMAIL,
            MESSAGE_CONFIRM.format(url),
            settings.EMAIL_HOST_USER,
            [user_email],
            connection=connection,
        ).send()
        
        
