<h1>Тестовое задание "Сервис тестов"</h1>
<ul>
    <ol>Создать файл .env в корне рабочей директории</ol>
    <ol>Заполнить его вот так
    <code>
    EMAIL_HOST_USER = example@gmail.com
    EMAIL_HOST_PASSWORD= password from smtp server
    DB_NAME = postgres
    DB_USER = postgres
    DB_PASSWORD = postgres
    DB_HOST = db_test_service
    DB_PORT = 5432 # НЕ МЕНЯЕМ!
    SERVICE_PORT = 8000
    SUPERUSER = some_name
    SUPERUSER_EMAIL = example@gmail.com
    DJANGO_SUPERUSER_PASSWORD = some_password
    </code>
    </ol>
    <ol>При возможных проблемах с psycopg2-binary скачать psycopg2</ol>
    <ol>Запустить команду в терминале <code>sudo docker build -t django_custom_image .</code>
    (Обращаю внимание, что вводить нужно точь в точь, потому что на django_custom_image ссылается файл docker-compose.yml)
    (Также обращаю внимание, что sudo нужно вводить, если ваша ОС - linux, на Windows - не нужно)
    </ol>
    <ol>Затем ввести команду <code>sudo docker compose up</code></ol>
    <ol></ol>
    <ol></ol>
    <ol></ol>
    <ol></ol>
    <ol></ol>
    <ol></ol>
    <ol></ol>
    <ol></ol>
<ul>